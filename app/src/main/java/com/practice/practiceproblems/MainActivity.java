package com.practice.practiceproblems;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ListActivity implements AdapterView.OnItemClickListener{

    boolean viewingProblems = false;
    ListAdapter adapter;
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        adapter = new ListAdapter(getResources().getStringArray(R.array.section_names), this);
        getListView().setAdapter(adapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (!viewingProblems){
            setProblemList(view);
            tvTitle.setText("Select a Problem");

        }else{
            Bundle problemInformation = getProblemInformation(view);
            Intent intent = new Intent(this, DisplayProblem.class);
            intent.putExtras(problemInformation);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        adapter.listElements = MainActivity.this.getResources().getStringArray(R.array.section_names);
        adapter.notifyDataSetChanged();
        viewingProblems = false;
        tvTitle.setText("Select a Topic");
    }

    private void setProblemList(View view) {
        String listItem = adapter.viewMap.get(view);
        if(listItem.equals("Arrays and Strings")){
            adapter.listElements = getResources().getStringArray(R.array.arrays_and_strings);

        }else if(listItem.equals("Linked Lists")){
            adapter.listElements = getResources().getStringArray(R.array.linked_lists);
        }
        viewingProblems = true;
        adapter.notifyDataSetChanged();
    }

    //TODO
    private Bundle getProblemInformation(View view) {
        String problemName = adapter.viewMap.get(view);
        Bundle data = new Bundle();
        String name = "null", description = "null", solution = "null";

        //Close block {} CTRL + SHIT + PERIOD
        if(problemName.equals("Remove duplicates")){
            name = problemName;
            description = "Write code to remove duplicates from an unsorted Linked List(ex. FOLLOW UP).\n How would you solve this" +
                    " problem if a temporary buffer is not allowed?";
            solution = "\tpublic void removeCopiesLinearTime(){\n" +
                    "\t\tHashMap<Integer, Integer> map = new HashMap<Integer, Integer>();\n" +
                    "\t\tNode curr = this;\n" +
                    "\t\tmap.put(curr.data, 1);\n" +
                    "\t\t\n" +
                    "\t\twhile(curr.next != null){\n" +
                    "\t\t\tif(map.containsKey(curr.next.data)){\n" +
                    "\t\t\t\tif(curr.next.next != null){\n" +
                    "\t\t\t\t\tcurr.next = curr.next.next;\n" +
                    "\t\t\t\t}else{\n" +
                    "\t\t\t\t\tcurr.next = null;\n" +
                    "\t\t\t\t}\n" +
                    "\t\t\t\t\n" +
                    "                map.put(curr.next.data, 1);\n" +
                    "\t\t\t}else{\n" +
                    "                map.put(curr.next.data, 1);\n" +
                    "\t\t\t}\n" +
                    "\t\t\t\n" +
                    "\t\t\tcurr = curr.next;\n" +
                    "\t\t}\n" +
                    "\t}\n" +
                    "\t\n" +
                    "\tpublic void removeCopiesConstantSpace(){\n" +
                    "\n" +
                    "        Node hold = this;\n" +
                    "\t\tNode check;\n" +
                    "\n" +
                    "\t\tdo{\n" +
                    "\t\t\t\n" +
                    "\t\t\tcheck = hold;\n" +
                    "\t\t\twhile(check.next != null){\n" +
                    "\t\t\t\thold.printNode();\n" +
                    "\t\t\t\tif(hold.data == check.next.data){\n" +
                    "\t\t\t\t\tif(check.next.next != null){\n" +
                    "\t\t\t\t\t\tcheck.next = check.next.next;\n" +
                    "\t\t\t\t\t}else{\n" +
                    "\t\t\t\t\t\tcheck.next = null;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t\t\t\tcontinue;\n" +
                    "\t\t\t\t}\n" +
                    "\t\t\t\t\n" +
                    "\t\t\t\tcheck = check.next;\n" +
                    "\t\t\t}\n" +
                    "\t\t\t\n" +
                    "\t\t\thold = hold.next;\n" +
                    "\t\t}while(hold != null);\n" +
                    "\t}";
        }
        if(problemName.equals("Kth to last element")){
            name = "Kth to last element";
            description = "Implement an algorithm to find the kth to last element in a singly linked list";
            solution = "\tpublic Node findKthToLast(int k){\n" +
                    "\t\tNode front = this, back = this;\n" +
                    "\t\tfor(int i = 0; i < k; i++){\n" +
                    "\t\t\tfront = front.next;\n" +
                    "\t\t}\n" +
                    "\t\t\n" +
                    "\t\tdo{\n" +
                    "\t\t\tfront = front.next;\n" +
                    "\t\t\tback = back.next;\n" +
                    "\t\t}while(front != null);\n" +
                    "\t\t\n" +
                    "\t\treturn back;\n" +
                    "\t}";
        }
        if(problemName.equals("Remove current Node")){
            name = "Remove current Node";
            description = "Implement an algorithm to remove a node from a linked list. (You only have reference to the input node)";
            solution = "\tpublic boolean removeThisNode(Node node){\n" +
                    "\t\tif(node == null || node.next == null){\n" +
                    "\t\t\treturn false;\n" +
                    "\t\t}\n" +
                    "\t\t\n" +
                    "\t\tNode n = node.next;\n" +
                    "\t\tnode.data = n.data;\n" +
                    "\t\tnode.next = n.next;\n" +
                    "\t\tnode.next = n.next;\n" +
                    "\t\treturn true;\n" +
                    "\t}";
        }
        if(problemName.equals("Partition around value")){
            name = "Partition around value";
            description = "Write code to partition a linked list such that all elements greater than x come after all elements less than x." +
                    "\n EX:\n" +
                    "input linked list: 10-5-4-2-3-6-8-7" +
                    "\n output linked list: 2-3-10-5-4-6-8-7";
            solution = "\tpublic Node partitionAround(Node node, int x){\n" +
                    "\t\t\n" +
                    "\t\tNode beforeStart = null;\n" +
                    "\t\tNode beforeEnd = null;\n" +
                    "\t\tNode afterStart = null;\n" +
                    "\t\tNode afterEnd = null;\n" +
                    "\t\t\n" +
                    "\t\twhile(node != null){\n" +
                    "\t\t\n" +
                    "\t\t\tif(node.data >= x){\n" +
                    "\t\t\t\t\n" +
                    "\t\t\t\tif(afterStart == null){\n" +
                    "\t\t\t\t\tafterStart = node;\n" +
                    "\t\t\t\t\tafterEnd = afterStart;\n" +
                    "\t\t\t\t}else{\n" +
                    "\t\t\t\t\tafterEnd.next = node;\n" +
                    "\t\t\t\t\tafterEnd = node;\n" +
                    "\t\t\t\t}\n" +
                    "\t\t\t}else{\n" +
                    "\t\t\t\t\n" +
                    "\t\t\t\tif(beforeStart == null){\n" +
                    "\t\t\t\t\tbeforeStart = node;\n" +
                    "\t\t\t\t\tbeforeEnd = beforeStart;\n" +
                    "\t\t\t\t}else{\n" +
                    "\t\t\t\t\tbeforeEnd.next = node;\n" +
                    "\t\t\t\t\tbeforeEnd = node;\n" +
                    "\t\t\t\t}\n" +
                    "\t\t\t}\n" +
                    "\t\t\t\n" +
                    "\t\t\tnode = node.next;\n" +
                    "\t\t}\n" +
                    "\t\t\n" +
                    "\t\tif(beforeStart == null){\n" +
                    "\t\t\treturn afterStart;\n" +
                    "\t\t}else{\n" +
                    "\t\t\tbeforeEnd.next = afterStart;\n" +
                    "\t\t\treturn beforeStart;\n" +
                    "\t\t}\n" +
                    "\t}";
        }
        if(problemName.equals("Linked List Sum")){
            name = "Linked List Sum";
            description = "You have two numbers represented by a linked list, where each node contains a single digit. The digits are sorted in reverse order, such that the 1's" +
                    "digit is at the head of the list. Write a function that adds the two numbers and returns the sum as a linked list.";
            solution = "\tpublic static Node linkedListSum(Node n1, Node n2){\n" +
                    "\t\tNode sumNode = new Node(0);\n" +
                    "\t\tNode ret = sumNode;\n" +
                    "\t\t\n" +
                    "\t\tint sum, excess = 0;\n" +
                    "\t\t\n" +
                    "\t\twhile(sumNode != null){\n" +
                    "\t\t\tsum = excess;\n" +
                    "\t\t\tif(n1 != null){ sum += n1.data; n1 = n1.next; }\n" +
                    "\t\t\tif(n2 != null){ sum += n2.data; n2 = n2.next; }\n" +
                    "\t\t\tif(sum >= 10){\n" +
                    "\t\t\t\tsumNode.next = new Node(0);\n" +
                    "\t\t\t\tsumNode.data = sum%10;\n" +
                    "\t\t\t\texcess = (sum - sum%10)/10;\n" +
                    "\t\t\t}else{\n" +
                    "\t\t\t\tsumNode.data = sum;\n" +
                    "\t\t\t}\n" +
                    "\t\t\t\n" +
                    "\t\t\tsumNode = sumNode.next;\n" +
                    "\t\t}\n" +
                    "\t\t\n" +
                    "\t\treturn ret;\n" +
                    "\t}";
        }
        if(problemName.equals("Find the Loop")){
            name = "Find the Loop";
            description = "Given a circular linked list, return the node that is at the beginning of a loop.";
            solution = "\tpublic static Node startOfLoop(Node node){\n" +
                    "\t\tHashMap<Node, Boolean> map = new HashMap<Node, Boolean>();\n" +
                    "\t\t\n" +
                    "\t\twhile(!map.containsKey(node)){\n" +
                    "\t\t\tmap.put(node, true);\n" +
                    "\t\t\tnode = node.next;\n" +
                    "\t\t}\n" +
                    "\t\t\n" +
                    "\t\tSystem.out.println(node.data);\n" +
                    "\t\treturn node;\n" +
                    "\t}";
        }



        data.putString("name", name);
        data.putString("description", description);
        data.putString("solution", solution.replace("\t", "\t\t\t"));

        return data;
    }
}
