package com.practice.practiceproblems;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Allen on 11/12/2014.
 */
public class ListAdapter extends BaseAdapter {

    String[] listElements;
    HashMap<View, String> viewMap;
    Context context;

    public ListAdapter(String[] elements, Context context){
        listElements = elements;
        this.context = context;
        viewMap = new HashMap<View, String>();
    }

    @Override
    public int getCount() {
        return listElements.length;
    }

    @Override
    public Object getItem(int position) {
        return listElements[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.row_element, parent, false);
        TextView tvTitle = (TextView) row.findViewById(R.id.tvTitle);
        tvTitle.setText(listElements[position]);
        viewMap.put(row, listElements[position]);
        return row;
    }
}
