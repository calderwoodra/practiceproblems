package com.practice.practiceproblems;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Allen on 11/12/2014.
 */
public class DisplayProblem extends Activity{

    TextView tvProblemName, tvProblemDescription, tvProblemSolution;
    Button bShowSolution;
    boolean solutionIsVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.problem_layout);

        Bundle problemInformation = getIntent().getExtras();

        tvProblemName = (TextView) findViewById(R.id.tvProblemName);
        tvProblemSolution = (TextView) findViewById(R.id.tvSolution);
        tvProblemDescription = (TextView) findViewById(R.id.tvProblemDescription);
        bShowSolution = (Button) findViewById(R.id.bSolution);

        tvProblemName.setText(Html.fromHtml("<h1>" + problemInformation.getString("name") + "</h1>"));
        tvProblemDescription.setText(problemInformation.getString("description"));
        tvProblemSolution.setText(problemInformation.getString("solution"));
        bShowSolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(solutionIsVisible){
                    tvProblemSolution.setVisibility(View.INVISIBLE);
                    bShowSolution.setText("Show Solution");
                    solutionIsVisible = false;
                }else{
                    tvProblemSolution.setVisibility(View.VISIBLE);
                    bShowSolution.setText("Hide Solution");
                    solutionIsVisible = true;
                }
            }
        });
    }
}
